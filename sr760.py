#!/usr/bin/env python
# -*- mode: Python; coding: utf-8 -*-
# Time-stamp: "2016-09-21 17:40:00 sb"

#  file       sr760.py
#  copyright  (c) Sebastian Blatt 2015, 2016

import six
import sys, os

import socket
import select
import time
from datetime import datetime
import re
import argparse
import numpy as np

import tools.visualization as vis

PROGRAM_VERSION = '160921'

class Sr760(object):
  # Valid frequency span ids from manual p. 5-4
  FREQUENCY_SPANS = {0: 191e-3,
                     1: 382e-3,
                     2: 763e-3,
                     3: 1.5,
                     4: 3.1,
                     5: 6.1,
                     6: 12.2,
                     7: 24.4,
                     8: 48.75,
                     9: 97.5,
                     10: 195,
                     11: 390,
                     12: 780,
                     13: 1.56e3,
                     14: 3.125e3,
                     15: 6.25e3,
                     16: 12.5e3,
                     17: 25e3,
                     18: 50e3,
                     19: 100e3}

  DISPLAY_UNITS = {0: "Vpk",
                   1: "Vrms",
                   2: "dBV",
                   3: "dBVrms"}

  AVERAGING_TYPES = {0: 'rms',
                     1: 'vector',
                     2: 'peak hold'}

  AVERAGING_MODES = {0: 'lin',
                     1: 'exp'}

  def __init__(self, ipv4_address, gpib_address):
    self.ipv4_address = ipv4_address
    self.port = 1234
    self.timeout = 2
    self.gpib_address = gpib_address
    self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

  def connect(self):
    self.socket.connect((self.ipv4_address, self.port))

  def close(self):
    self.set_local()
    self.socket.close()

  def send(self, data, termchar = "\r\n"):
    x = data + termchar
    if six.PY3:
      x = bytes(x, 'ascii')
    self.socket.send(x)

  def receive_select(self, timeout = 0.1, Timeout = 5, max_size = 4096, term_sequence = '\n'):
    """Use select and non-blocking socket to receive data. Each call to
    select waits for a maximum of `max_size' bytes and times out with
    `timeout' seconds. The overall loop exits after `Timeout' seconds.

    Break the loop when receiving `term_sequence' and return a
    concatenated string containing all received data, but strip
    `term_sequence' from the end.
    """
    # It seems to work more reliably on OSX to have the socket be
    # blocking, and only non-blocking when you really want it.
    self.socket.setblocking(0)

    rc = []
    t0 = time.time()
    while True:
      ready = select.select([self.socket], [], [], timeout)
      if ready[0]:
        buf = self.socket.recv(max_size)
        rc.append(buf)

        #print("recv -> \"%s\"" % buf)
        trm = buf[-len(term_sequence):]
        if six.PY3:
          trm = trm.decode('ascii')
        if trm == term_sequence:
          break

      if time.time() - t0 > Timeout:
        print("! Receiver timed out after %g s.\n" % Timeout)
        break

    self.socket.setblocking(1)
    if six.PY3:
      rc = [x.decode('ascii') for x in rc]
    data = ''.join(rc)
    return data[0:len(data)-len(term_sequence)]

  def query(self, data):
    self.send(data)
    self.send('++read')
    #return self.receive()
    return self.receive_select(Timeout = 7.0)


  def setup_prologix(self):
    # Disable auto-saving of settings to prevent EPROM wear
    # on every connection.
    self.send('++savecfg 0')

    # We always want to be the GPIB controller
    self.send('++mode 1')
    self.send('++eoi 1')

    # Turn off auto-reply, i.e. the GPIB device will not talk back to
    # the controller without '++read' commands.
    self.send('++auto 0')

    # Set termchar to LF only for instrument commands
    self.send('++eos 3')

    # Set GPIB address to address on device
    self.send('++addr %i' % self.gpib_address)

    # Read timeout
    self.send('++read_tmo_ms 1000')

  def download_spectrum(self, trace = 1):
    data = self.query('SPEC? %d' % (trace - 1))
    return data

  def get_frequency_range(self):
    """Return results of (STRF?, CTRF?, SPAN?) in units of Hz."""
    strf = float(self.query('STRF?'))
    ctrf = float(self.query('CTRF?'))
    span_id = int(self.query('SPAN?'))
    span = Sr760.FREQUENCY_SPANS[span_id]
    return (strf, ctrf, span)

  def get_units(self, trace = 1):
    unit_id = int(self.query('UNIT? %d' % (trace - 1)))
    return (unit_id, Sr760.DISPLAY_UNITS[unit_id])

  def get_averaging(self):
    is_averaging = int(self.query('AVGO?')) == 1
    n_averages = int(self.query('NAVG?'))
    avg_type_id = int(self.query('AVGT?'))
    avg_type = Sr760.AVERAGING_TYPES[avg_type_id]
    avg_mode_id = int(self.query('AVGM?'))
    avg_mode = Sr760.AVERAGING_MODES[avg_mode_id]
    return (is_averaging, n_averages, avg_type, avg_mode)

  def set_local(self):
    self.send('++loc')

def parse_command_line():
  parser = argparse.ArgumentParser(description = "SR760 Communication")
  parser.add_argument('-o', '--output',
                      help = 'output file',
                      default = 'test.txt')
  parser.add_argument('-a', '--address',
                      help = 'IPV4 address of GPIB adapter',
                      default = '192.168.0.185')
  parser.add_argument('-g', '--gpib',
                      help = "GPIB address of the device. Found/Set via 'Setup > Communication > Setup GPIB'.",
                      default=10)
  parser.add_argument('-v', '--version',
                      action = 'version',
                      version = PROGRAM_VERSION)
  args = parser.parse_args()
  return args

def save_trace(filename : str, values, add_prefix = True):

  if add_prefix:
    now = datetime.now()
    output_path = os.path.join("data" , now.strftime("%Y/%m/%d"))
    if not os.path.exists(output_path): os.makedirs(output_path)
    filename = os.path.join(output_path, filename)

  with open(filename, 'w') as f:
    for j in range(len(values)):
      f.write("%03d\t%g\t%s\n" % (j, fourier_freqs[j], values[j]))


if __name__ == "__main__":
  args = parse_command_line()

  s = Sr760(args.address, int(args.gpib))
  s.connect()
  s.setup_prologix()
  print("Connected to %s." % args.address)


  x = s.download_spectrum()
  values = re.split(r',', x)
  values = values[0:len(values)-1]
  print(np.array(values))

  fs = s.get_frequency_range()
  us = s.get_units()
  avs = s.get_averaging()

  # Represent frequency bins by the left (lower) edge! The spectrum
  # analyzer always returns 400 bins. The lowest bin contains [strf,
  # strf + delta] and the highest bin contains [strf + span - delta,
  # strf + span].
  fourier_freqs = np.linspace(fs[0], fs[2], 400, endpoint = False)

  print(fs)
  print(us)
  print(avs)

  s.close()

  vis.plot_trace(values, fourier_freqs)
  save_trace(args.output, values)



# sr760.py ends here
