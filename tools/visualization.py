import matplotlib.pyplot as plt
import numpy as np
import os

def plot_trace(values, freqs, label = None):

    values = np.array(values, dtype=float)

    # labels
    plt.xlabel(r'frequency (Hz)')
    plt.ylabel(r'power $[\frac{dBV}{\sqrt{Hz}}]$')

    if label is not None:
        plt.plot(freqs, values, label = label)
    else:
        plt.plot(freqs, values)
        plt.show()
    return

def plot_from_file(filename):

    data = np.genfromtxt(filename, dtype=float).T
    values = data[2]
    freqs = data[1]
    plot_trace(values, freqs, label=os.path.basename(filename))



